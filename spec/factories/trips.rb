FactoryGirl.define do
  factory :trip, class: Trip do
    title 'roadtrip'
    description 'this is a test roadtrip'
  end

  factory :invalid_trip, class: Trip do
    title nil
    description nil
  end
end

