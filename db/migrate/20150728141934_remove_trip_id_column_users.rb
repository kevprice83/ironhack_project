class RemoveTripIdColumnUsers < ActiveRecord::Migration
  def change
    remove_column :users, :trip_id, :integer
  end
end
