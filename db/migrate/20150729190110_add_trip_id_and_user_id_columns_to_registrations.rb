class AddTripIdAndUserIdColumnsToRegistrations < ActiveRecord::Migration
  def change
    add_column :registrations, :trip_id, :integer
    add_column :registrations, :user_id, :integer
  end
end
