class AddColumnsUsers < ActiveRecord::Migration
  def change
    add_column :users, :bio, :text
    add_column :users, :climbing_grade, :string
    add_column :users, :favourite_spots, :text
    add_column :users, :climbing_style, :text
  end
end
