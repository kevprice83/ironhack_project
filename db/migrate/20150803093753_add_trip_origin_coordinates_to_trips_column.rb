class AddTripOriginCoordinatesToTripsColumn < ActiveRecord::Migration
  def change
    add_column :trips, :origin, :float
    add_column :trips, :departure, :date
  end
end
