class CreateTripRegistrations < ActiveRecord::Migration
  def change
    create_table :trip_registrations do |t|
      t.string :status
      t.integer :user_id
      t.integer :trip_id

      t.timestamps null: false
    end
  end
end
