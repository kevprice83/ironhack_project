# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

puts " --- Deleting all Users --- "
# this does also delete all trips --> Dependent destroy

User.destroy_all

def title_generator
  words = %w(rock road climb trip camp journey)

  "#{words.sample}#{words.sample}#{words.sample}"
end

def rand
  neg = Random.rand > 0.5
  r = Random.rand
  if(neg)
    r = r * -1
  end
  return r / 55
end

def create_trip_for(user)
  Trip.create!(
    driver: user,
    title: title_generator,
    description: "some description",
    latitude: 41.3917782 + rand,
    longitude: 2.1772809999999936 + rand,
    departure: Date.current + Random.rand(14)
  )
end

%w(Unai Kevin Jacobo Llorenc Andreu).each do |name|
  user = User.create!(name: name, email: "#{name}@gmail.com", password: "12345678", password_confirmation: "12345678")
    2.times do
      create_trip_for(user)
    end
end

