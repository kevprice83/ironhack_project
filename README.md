# [Gri-gri](https://grigri.herokuapp.com/) #

### Final Project @ IronHack ###


This is my first web app. I built it in 2 weeks after completing 6 weeks of theory in Ruby on Rails, JavaScript and HTML5.

### Technologies used ###

![alt text](http://www.icon100.com/up/2998/128/868-development-ruby-on-rails.png)
![alt text](https://upload.wikimedia.org/wikipedia/commons/f/f1/Ruby_logo_64x64.png)
![alt text](http://www.przemyslawolesinski.pl/assets/rspec-938a01103923a71e8f0c785985c720e0.jpg)
![alt text](http://s3.amazonaws.com/cloud.ohloh.net/attachments/80491/git-logo_med.jpg)
![alt text](http://www.ana2lp.mx/wp-content/uploads/2014/05/2014-06-26-HTML_CSS_JS_logo.png)

***Why I chose to build this application:*** I'm a climber. So it was important that I would build a project that I was interested in and passionate about. A lot of late nights were involved in the last few weeks. Secondly, I wanted to implement all that I had learned during the bootcamp, although my other idea had greater feedback from my peers it just wouldn't have enabled me to consolidate what I had used over the course.

***What did I learn:*** No matter how close you think you are to a finished product, the list will always grow. I have continued to work on my project since the bootcamp ended and the TODO list keeps growing. I learned how to prioritise tasks and how to tackle these in an effective manner using the SCRUM approach, this was really useful in visualising the different stages of the application and it highlights tasks which maybe weren't prioritised in the beginning.

***Testing:*** Start with good intentions...this is how I approached testing. I didn't get anywhere near the coverage I would have liked but I decided to test the controllers which we hadn't covered during the bootcamp so this turned out to be quite challenging for me. As always perseverance is key and I felt immense satisfaction in acheiving just a small coverage of testing and learning how to use FactoryGirl along the way (even if it was unnecessary).

###Instructions to run the app: ###

clone the project
```
git clone https://kevprice83@bitbucket.org/kevprice83/ironhack_project.git
```

install the ruby dependencies
```
bundle install
```

install the node dependencies
```
npm install
```

run the migrations
```
rake db:migrate
```


run the server
```
rails s
```


to run the specs:
```
rake spec
```