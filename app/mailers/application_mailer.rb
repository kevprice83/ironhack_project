class ApplicationMailer < ActionMailer::Base
  default from: "info.grigri@gmail.com"
  layout 'mailer'
end
