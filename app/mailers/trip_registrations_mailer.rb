class TripRegistrationsMailer < ApplicationMailer

  default from: 'info.grigri@gmail.com'

  def join_trip_mail(trip_registration)
    @trip_registration = trip_registration
    mail(to: @trip_registration.trip.driver.email, subject: "You have a new request for #{@trip_registration.trip.title}")
    mail(to: @trip_registration.passenger.email, template_path: 'trip_registrations_mailer', template_name: 'request_to_join_trip_mail', subject: "Your request has been successful for #{@trip_registration.trip.title}")
  end

  def accept_request_mail(trip_registration)
    @trip_registration = trip_registration
    mail(to: @trip_registration.passenger.email, subject: "Your request has been accepted for #{@trip_registration.trip.title}")
  end

  def decline_request_mail(trip_registration)
    @trip_registration = trip_registration
    mail(to: @trip_registration.passenger.email, subject: "Your request has been declined for #{@trip_registration.trip.title}")
  end
end
