class LandingController < ApplicationController

  def index
    if current_user.present?
      redirect_to home_path
    end
    @new_user_session = User.new
  end
end
