class TripsController < ApplicationController
  before_action :authorize_user

  def index
    @trips = Trip.where("departure >= :today", today: Time.now)
    @next_trips = Trip.later_this_month 
  end

  def new
    @trip = current_user.trips.new
  end

  def create
    @trip = current_user.trips.new(trip_params)
    if @trip.valid?
      @trip.save
      flash[:success] = "You have successfully created a trip, Better get training for your next project!"
      redirect_to trip_path(@trip)
    else
      flash[:danger] = "All fields need to be completed to create a trip."
      render :new
    end
  end

  def show
    unless current_user
      flash[:warning] = "Please sign up or log in to view this page."
      redirect_to new_user_registration_path
    end
    @trip = Trip.find(params[:id])
    @driver = @trip.driver

    @trip_registrations = @trip.trip_registrations

     # @whos_going = @trip.regs[:accepted].collect {|r| r.passenger } + @trip.driver

  end


  def destroy
    @trip = Trip.find(params[:id])
    @trip.destroy
    flash[:info] = "You have just deleted a trip."
    redirect_to trips_path
  end

  def trip_params
    params.require(:trip).permit(:title, :description, :latitude, :longitude, :departure, :destination)
  end

end
