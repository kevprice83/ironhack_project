class UsersController < ApplicationController
  before_action :authenticate_user!, only: :show

  def index
    @users = User.all
  end

  def edit
    @user = User.find(params[:id])
  end

  def create

  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes(user_params)
    if @user.save
      redirect_to user_path(@user)
      return
    end

    render :edit
  end

  def show
    @user = User.find_by(id: params[:id])
  end

  def user_trip_search
    @trips = current_user.trips
  end

  

  def user_params
    params.require(:user).permit(:bio, :email, :climbing_grade, :climbing_style, :favourite_spots, :role)
  end

end
