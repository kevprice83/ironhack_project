class Api::TripsController < ApplicationController

  def index
    @trips = Trip.where("departure >= :today", today: Time.now)
    render status: 200, json: @trips
  end

  def show
    @trip = Trip.find(params[:id])
    render status: 200, json: @trip
  end
end
