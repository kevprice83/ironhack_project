class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessor :current_password

  # validates :name, presence: true
  validates :email, presence: true, uniqueness: true
  validates :password, presence: true, on: :create
  validates_length_of :password, minimum: 8, maximum: 15, on: :update, allow_blank: true

  has_many :trips, dependent: :destroy
  has_many :trip_registrations, dependent: :destroy
  has_many :lifts, through: :trip_registrations, source: :trip
  

  def on_trip?(trip)
    self.trip_registrations.where(trip_id: trip.id).size > 0
  end

  def trip_status(trip)
    self.trip_registrations.find_by(trip_id: trip.id).status
    
  end

  def later_this_month(date = Date.current)
    self.trips.where("departure >= ?", date.end_of_month).limit(5)
  end

  def upcoming(date = Date.current)
    self.trips.where("departure >= ?", date).limit(2)
  end

  def soon(date = Date.current)
    self.lifts.where("departure >= ?", date).limit(2)
  end

end
