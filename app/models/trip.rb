class Trip < ActiveRecord::Base
  validates :title, presence: true
  validates :description, presence: true
  validates :departure, presence: true
  validates :destination, presence: true

  belongs_to :driver, class_name: "User", foreign_key: :user_id
  has_many :trip_registrations, dependent: :destroy
  has_many :passengers, class_name: "User", through: :trip_registrations

  default_scope { order('departure ASC') }

  def self.trip_find(date = Date.current)
    order("title ASC").where("created_at > ?", date).limit(10)
  end

  def regs
    p = { accepted:[] }
    p[:total] = trip_registrations.length
    trip_registrations.to_ary.each do |r|
      puts "p[r.status]=#{p[r.status]}"
      p[r.status.to_sym] ||= []
      p[r.status.to_sym] << r
    end
    return p
  end

  def self.later_this_month(date = Date.current)
    Trip.all.where("departure BETWEEN ? and ?", date.beginning_of_day, date.end_of_month).limit(8)
  end

  def self.most_popular
    Trip.joins(:trip_registrations).group("trips.id").order("COUNT (*) DESC ").limit(5).count
  end

end
