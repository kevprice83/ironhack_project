$(function () {
  var updateRegistration = function(event) {
    event.preventDefault();

    var regId = $(event.currentTarget).data("registration-id");

    var statusUpdate = $(event.currentTarget).data("status");

    var url = "/trip_registrations/" + regId;

    function handleSuccess(response) {
      if($('#status' + regId).prop('id') === 'status' + regId) {
        $('#status' + regId).text(statusUpdate)
        $('span#status' + regId).prop('class', 'registration-' + statusUpdate)
      };
    }

    function handleError(response) { 
    }

    $.ajaxSetup({
    headers: {
      'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
    }
    });

    $.ajax({
      url: url,
      type: 'put',
      data: {status: statusUpdate},
      success: handleSuccess,
      error: handleError,
      datatype: "json"
    });
  }


$('.registration-process').on('click', updateRegistration);

});
