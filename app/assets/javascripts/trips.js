$(function() {

  var geocoder;
  var map;
  var infowindow;
  var marker;
  window.createTripInitializeMap = function () {
    geocoder = new google.maps.Geocoder;
    infowindow = new google.maps.InfoWindow;
    var mapOptions = {
      zoom: 10,
      center: new google.maps.LatLng(41.3917782, 2.1772809999999936)
    };

    map = new google.maps.Map(document.getElementById('new-trip-map'), mapOptions);
    google.maps.event.addListener(map, 'click', function(event) {
      var lat = event.latLng.lat();
      var lon = event.latLng.lng();
      marker = addMarker(lat, lon);
      $('#latitude').val(lat);
      $('#longitude').val(lon);
      codeLatLng(geocoder, map, infowindow);
    });

  };

  function codeLatLng(geocoder, map, infowindow) {
    var lat = $('#latitude').val();
    var lng = $('#longitude').val();

    var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

    geocoder.geocode({'location': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          map.setZoom(18);
          $('#address').val(results[0].formatted_address);
        } else {
          window.alert('no results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
    });
  }

  function addMarker(lat, lon) {
    var position = new google.maps.LatLng(lat, lon);
    var marker = new google.maps.Marker( {
      position: position,
      animation: google.maps.Animation.DROP,
      draggable: true,
      title: "Drag me!"
    });
    marker.setMap(map);
    map.setCenter(position);
  };

  $('#submit').on("click", getAddress);

  function getAddress(event) {
    event.preventDefault();
    var address = document.getElementById("address").value;
    getCoords(address);
  };

  function getCoords (address) {
    geocoder.geocode( { 'address' : address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var origin = results[0].geometry.location;
        var marker = new google.maps.Marker({
          map: map,
          position: origin
        });
        $('#latitude').val(results[0].geometry.location.lat());
        $('#longitude').val(results[0].geometry.location.lng());
      } else {
        alert("There was something wrong with your address");
      };
    });
  };
  
})
